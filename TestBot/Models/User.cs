﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestBot.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string DateOfBirth { get; set; }
        public string IdDialog { get; set; }
    }
}