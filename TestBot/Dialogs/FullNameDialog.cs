﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;
using TestBot.Models;

namespace TestBot.Dialogs
{
    [Serializable]
    public class FullNameDialog : IDialog<string>
    {
        private int attempts = 3;

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Напишите ваше полное имя в формате: Фамилия Имя Отчество");
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;

            /* Если возвращаемое сообщение является валидным, отправить его в вызывающий диалог. */
            if ((message.Text != null) && (message.Text.Trim().Length > 0))
            {
                using (UserContext db = new UserContext())
                {
                    User user = new User()
                    {
                        FullName = message.Text,
                        IdDialog = message.Conversation.Id
                    };
                    db.Users.Add(user);
                    db.SaveChanges();
                }
                /* Завершение диалога, удаление его из стека диалога, и возвращение результата 
                    * в родительский/вызывающий диалог. */
                context.Done(message.Text);

            }
            /* Повторить попытку, повторить запрос пользователю */
            else
            {
                --attempts;
                if (attempts > 0)
                {
                    await context.PostAsync("Простите, я Вас не понимаю. Напишите свои ФИО (например, 'Самсонов Владислав Андреевич'");

                    /* Вызов этого же метода заново после получения ответа на вопрос выше*/
                    context.Wait(this.MessageReceivedAsync);
                }
                else
                {
                    /* Рушит текущий диалог, удаляет его из стека диалога, и выкидывает ошибку
                     * родительскому/вызывающему диалогу */
                    context.Fail(new TooManyAttemptsException("Неверный формат сообщения или пустая строка."));
                }
            }
        }


    }
}