﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using TestBot.Models;

namespace TestBot.Dialogs
{
#pragma warning disable 1998

    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            /* Ждет когда первое сообщение будет получено из беседы и потом вызовет MessageReceviedAsync 
             *  для обработки этого сообщения */
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            /* Когда MessageReceivedAsync будет вызван, он передает IAwaitable<IMessageActivity>.
            *  Для получения сообщения, ожидается result. */
            var message = await result;

            await SendWelcomeMessageAsync(context);
        }

        private async Task SendWelcomeMessageAsync(IDialogContext context)
        {
            /* Отсылает сообщение пользователю */
            await context.PostAsync("/reg - Регистрация.   /info - Информация о Вас   /delete - Удалить информацию о Вас");
            
            /* Вызывает CooperationDialog (некое сотрудничество, в котором обрабатываются команды)
             * и после вызывает NameDialogResumeAfter */
            context.Call(new CollaborationDialog(), CollaborationDialogResumeAfter);
        }

        private async Task CollaborationDialogResumeAfter(IDialogContext context, IAwaitable<string> result)
        {
           
        }
    }
}