﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;
using TestBot.Models;

namespace TestBot.Dialogs
{
    [Serializable]
    public class AgeDialog: IDialog<string>
    {
        private int attempts = 3;

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Напишите дату своего рождения в формате: дд.мм.гггг");
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;

            /* Если возвращаемое сообщение является валидным, отправить его в вызывающий диалог. */
            if ((message.Text != null) && (message.Text.Trim().Length > 0) && (DateTime.TryParse(message.Text, out DateTime date)))
            {
                /* Добавление в бд даты. Завершение диалога, удаление его из стека диалога, и возвращение результата 
                 * в родительский/вызывающий диалог. */
                addUserDateOfBirth(message);
                context.Done(message.Text);
            }
            /* Повторить попытку, повторить запрос пользователю */
            else
            {
                --attempts;
                if (attempts > 0)
                {
                    await context.PostAsync("Простите. Я не понимаю Ваш ответ. Напишите дату своего рождения. (например, '22.12.1995')");
                    context.Wait(this.MessageReceivedAsync);
                }
                else
                {
                    helpUnluckyUser(message);
                    await context.PostAsync("Слишком много попыток. Я решил поставить дату вашего рождения сам. Теперь вы родились 20.04.1889 :)");
                    context.Done(message.Text);
                }
            }
        }

        private void addUserDateOfBirth(IMessageActivity message)
        {
            using (UserContext db = new UserContext())
            {
                var userExist = db.Users.FirstOrDefault(e => e.IdDialog == message.Conversation.Id);
                userExist.DateOfBirth = message.Text;
                db.SaveChanges();
            }
        }

        private void helpUnluckyUser(IMessageActivity message)
        {
            using (UserContext db = new UserContext())
            {
                var userExist = db.Users.FirstOrDefault(e => e.IdDialog == message.Conversation.Id);
                userExist.DateOfBirth = "20.04.1989";
                db.SaveChanges();
            }
        }
    }
}