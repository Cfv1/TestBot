﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;
using TestBot.Models;

namespace TestBot.Dialogs
{
    [Serializable]
    public class CollaborationDialog : IDialog<string>
    {
        private string name;
        private string date;

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Напишите команду, пожалуйста");
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            if (message.Text != null)
            {
                if (message.Text == "/reg" || message.Text == "reg")
                {
                    using (UserContext db = new UserContext())
                    {
                        bool userExist = db.Users.Any(e => e.IdDialog == message.Conversation.Id);
                        if (userExist)
                        {
                            await context.PostAsync("Вы уже зарегистрированы. Напишете что угодно для продолжения");
                            context.Wait(MessageReceivedAsync);
                        }
                    } 
                    /* Вызов диалога для получения ФИО */
                    context.Call(new FullNameDialog(), FullNameDialogResumeAfter);

                }
                else if (message.Text == "/info" || message.Text == "info")
                {
                    string existing = сheckUserAvailability(message.Conversation.Id);
                    await context.PostAsync(existing);
                }
                else if (message.Text == "/delete" || message.Text == "delete")
                {
                    string delete = deleteUser(message.Conversation.Id);
                    await context.PostAsync(delete);
                }
                else
                {
                    await context.PostAsync("Я не знаю такой команды");
                    context.Wait(MessageReceivedAsync);
                }
            }
        }

        private async Task FullNameDialogResumeAfter(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                name = await result;

                /* Если вернулась не ошибка, идет вызов AgeDialog (диалог для получения возраста)
                 * и после завершения вызывает продолжение, а именно - AgeDialogResumeAfter*/
                context.Call(new AgeDialog(), AgeDialogResumeAfter);
            }
            catch (TooManyAttemptsException)
            {
                /* Если словил error */
                await context.PostAsync("Простите, у меня проблемы пониманием. Давайте попробуем заново.");

                /* Начинает диалог заново */
                await StartAsync(context);
            }
        }

        private async Task AgeDialogResumeAfter(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                this.date = await result;
                await context.PostAsync($"Будем знакомы, {name}");

            }
            catch (TooManyAttemptsException)
            {

                await context.PostAsync("Начнем заново.");
            }
            finally
            {
                await this.StartAsync(context);
            }
        }

        private string сheckUserAvailability(string id)
        {
            string result;
            using (UserContext db = new UserContext())
            {
                var userExist = db.Users.FirstOrDefault(e => e.IdDialog == id);
                if (userExist != null)
                    result = $"Вас зовут: {userExist.FullName}, дата вашего рождения: {userExist.DateOfBirth}";
                else
                    result = "Вы пока ещё мистер Анонимус :)";
            }
            return result;
        }

        private string deleteUser(string id)
        {
            string result;
            using (UserContext db = new UserContext())
            {
                var userExist = db.Users.FirstOrDefault(e => e.IdDialog == id);
                if (userExist != null)
                {
                    db.Users.Remove(userExist);
                    db.SaveChanges();
                    result = "Вы удалены из базы данных";
                }
                else
                {
                    result = "Вас ещё нет в базе данных";
                }
            }
            return result;
        }


    }
}